/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * Shinies is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: GlisteringRabbitRenderer.java
 * Date: 2020-04-19 "Basic Shiny Entities"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package com.elusivedreams.shinies.entity.render;

import com.elusivedreams.shinies.Shinies;
import com.elusivedreams.shinies.entity.GlisteringRabbitEntity;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.RabbitRenderer;
import net.minecraft.entity.passive.RabbitEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class GlisteringRabbitRenderer extends RabbitRenderer
{
    public GlisteringRabbitRenderer(EntityRendererManager renderManagerIn)
    {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getEntityTexture(RabbitEntity entity)
    {
        return new ResourceLocation(Shinies.MOD_ID, "textures/entity/glistering_rabbit.png");
    }

    public static class RenderFactory implements IRenderFactory<GlisteringRabbitEntity>
    {
        @Override
        public EntityRenderer<? super GlisteringRabbitEntity> createRenderFor(EntityRendererManager manager)
        {
            return new GlisteringRabbitRenderer(manager);
        }
    }
}
