/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * Shinies is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: TotemEvokerRenderer.java
 * Date: 2020-04-19 "Basic Shiny Entities"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package com.elusivedreams.shinies.entity.render;

import com.elusivedreams.shinies.Shinies;
import com.elusivedreams.shinies.entity.TotemEvokerEntity;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.EvokerRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class TotemEvokerRenderer extends EvokerRenderer<TotemEvokerEntity>
{
    public TotemEvokerRenderer(EntityRendererManager renderManagerIn)
    {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getEntityTexture(TotemEvokerEntity entity)
    {
        return new ResourceLocation(Shinies.MOD_ID, "textures/entity/totem_evoker.png");
    }

    public static class RenderFactory implements IRenderFactory<TotemEvokerEntity>
    {
        @Override
        public EntityRenderer<? super TotemEvokerEntity> createRenderFor(EntityRendererManager manager)
        {
            return new TotemEvokerRenderer(manager);
        }
    }
}
