/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * Shinies is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: ValuableTraderEntity.java
 * Date: 2020-04-19 "Basic Shiny Entities"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package com.elusivedreams.shinies.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.merchant.villager.WanderingTraderEntity;
import net.minecraft.world.World;

public class ValuableTraderEntity extends WanderingTraderEntity
{
    public ValuableTraderEntity(EntityType<? extends WanderingTraderEntity> p_i50178_1_, World worldIn)
    {
        super(p_i50178_1_, worldIn);
    }
}
