/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * Shinies is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: ClientHandler.java
 * Date: 2020-04-19 "Basic Shiny Entities"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package com.elusivedreams.shinies.handler;

import com.elusivedreams.shinies.entity.render.*;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
@OnlyIn(Dist.CLIENT)
public class ClientHandler
{
    @SubscribeEvent
    public static void onFMLClientSetup(FMLClientSetupEvent event)
    {
        RenderingRegistry.registerEntityRenderingHandler(
                Registry.GLITTERING_CHICKEN_ENTITY, new GlitteringChickenRenderer.RenderFactory()
        );
        RenderingRegistry.registerEntityRenderingHandler(
                Registry.GOLDEN_COW_ENTITY, new GoldenCowRenderer.RenderFactory()
        );
        RenderingRegistry.registerEntityRenderingHandler(
                Registry.TOTEM_EVOKER_ENTITY, new TotemEvokerRenderer.RenderFactory()
        );
        RenderingRegistry.registerEntityRenderingHandler(
                Registry.GLIMMERING_PIG_ENTITY, new GlimmeringPigRenderer.RenderFactory()
        );
        RenderingRegistry.registerEntityRenderingHandler(
                Registry.GLISTERING_RABBIT_ENTITY, new GlisteringRabbitRenderer.RenderFactory()
        );
        RenderingRegistry.registerEntityRenderingHandler(
                Registry.VALUABLE_TRADER_ENTITY, new ValuableTraderRenderer.RenderFactory()
        );
    }
}
