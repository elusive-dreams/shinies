/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * Shinies is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Registry.java
 * Date: 2020-04-19 "Basic Shiny Entities"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package com.elusivedreams.shinies.handler;

import com.elusivedreams.shinies.Shinies;
import com.elusivedreams.shinies.entity.*;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.item.*;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class Registry
{
    public static final ItemGroup ITEM_GROUP = new ItemGroup(Shinies.MOD_ID)
    {
        @Override
        public ItemStack createIcon()
        {
            return new ItemStack(Items.GOLD_INGOT);
        }
    };

    private static final Item.Properties DEFAULT_ITEM_PROPS = new Item.Properties().group(ITEM_GROUP);

    private static final DeferredRegister<Block> BLOCKS = new DeferredRegister<>(ForgeRegistries.BLOCKS, Shinies.MOD_ID);
    private static final DeferredRegister<Item> ITEMS = new DeferredRegister<>(ForgeRegistries.ITEMS, Shinies.MOD_ID);
    private static final DeferredRegister<EntityType<?>> ENTITIES = new DeferredRegister<>(ForgeRegistries.ENTITIES, Shinies.MOD_ID);

    public static EntityType<GlitteringChickenEntity> GLITTERING_CHICKEN_ENTITY;
    public static EntityType<GoldenCowEntity> GOLDEN_COW_ENTITY;
    public static EntityType<TotemEvokerEntity> TOTEM_EVOKER_ENTITY;
    public static EntityType<GlimmeringPigEntity> GLIMMERING_PIG_ENTITY;
    public static EntityType<GlisteringRabbitEntity> GLISTERING_RABBIT_ENTITY;
    public static EntityType<ValuableTraderEntity> VALUABLE_TRADER_ENTITY;

    private static void registerEntity(String name, EntityType type, SpawnEggItem item)
    {
        ENTITIES.register(name, () -> type);
        ITEMS.register(name + "_spawn_egg", () -> item);
    }

    public static void register()
    {
        GLITTERING_CHICKEN_ENTITY = EntityType.Builder.create(
                GlitteringChickenEntity::new, EntityClassification.CREATURE
        ).build(Shinies.MOD_ID + ":glittering_chicken");
        registerEntity("glittering_chicken", GLITTERING_CHICKEN_ENTITY, new SpawnEggItem(
                GLITTERING_CHICKEN_ENTITY, 0xcc8e27, 0xf5cc27, DEFAULT_ITEM_PROPS
        ));

        GOLDEN_COW_ENTITY = EntityType.Builder.create(
                GoldenCowEntity::new, EntityClassification.CREATURE
        ).build(Shinies.MOD_ID + ":golden_cow");
        registerEntity("golden_cow", GOLDEN_COW_ENTITY, new SpawnEggItem(
                GOLDEN_COW_ENTITY, 0xcc8e27, 0xf5cc27, DEFAULT_ITEM_PROPS
        ));

        TOTEM_EVOKER_ENTITY = EntityType.Builder.create(
                TotemEvokerEntity::new, EntityClassification.CREATURE
        ).build(Shinies.MOD_ID + ":totem_evoker");
        registerEntity("totem_evoker", TOTEM_EVOKER_ENTITY, new SpawnEggItem(
                TOTEM_EVOKER_ENTITY, 0xcc8e27, 0xf5cc27, DEFAULT_ITEM_PROPS
        ));

        GLIMMERING_PIG_ENTITY = EntityType.Builder.create(
                GlimmeringPigEntity::new, EntityClassification.CREATURE
        ).build(Shinies.MOD_ID + ":glimmering_pig");
        registerEntity("glimmering_pig", GLIMMERING_PIG_ENTITY, new SpawnEggItem(
                GLIMMERING_PIG_ENTITY, 0xcc8e27, 0xf5cc27, DEFAULT_ITEM_PROPS
        ));

        GLISTERING_RABBIT_ENTITY = EntityType.Builder.create(
                GlisteringRabbitEntity::new, EntityClassification.CREATURE
        ).build(Shinies.MOD_ID + ":glistering_rabbit");
        registerEntity("glistering_rabbit", GLISTERING_RABBIT_ENTITY, new SpawnEggItem(
                GLISTERING_RABBIT_ENTITY, 0xcc8e27, 0xf5cc27, DEFAULT_ITEM_PROPS
        ));

        VALUABLE_TRADER_ENTITY = EntityType.Builder.create(
                ValuableTraderEntity::new, EntityClassification.CREATURE
        ).build(Shinies.MOD_ID + ":valuable_trader");
        registerEntity("valuable_trader", VALUABLE_TRADER_ENTITY, new SpawnEggItem(
                VALUABLE_TRADER_ENTITY, 0xcc8e27, 0xf5cc27, DEFAULT_ITEM_PROPS
        ));

        BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ENTITIES.register(FMLJavaModLoadingContext.get().getModEventBus());
    }
}
